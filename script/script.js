
//Задав три змінні
var x = 6;
var y = 14;
var z = 4;
document.write("<div class='zminni'>");
document.write("Задані змінні для кожного прикладу" + "</br>");
document.write("x = 6" + "</br>");
document.write("y = 14" + "</br>");
document.write("z = 4" + "</br>");
document.write("</div>");

//выводжу на екран результати з поясненнями
//перше рівняння
document.write("<div class='first'>");

document.write("Перший приклад" + "</br>");
document.write("x += y - x++ * z" + "</br>");

document.write("<div class='explanation'>");
document.write("<ol>");
document.write("<li>" + "Перша дія х++, але так як це постфіксний інкремент х залишаеться без зміни" + "</li>" + "</br>");
document.write("<li>" + "Друга дія х*z, так як х залишився 6 то 6*4=24" + "</li>" + "</br>");
document.write("<li>" + "Третя дія від у віднімаємо результат другої дії, 14-24=-10" + "</li>" + "</br>");
document.write("<li>" + "четверта дія присвоення суми -10+6=-4, тобто весь результат <span>-4</span>" + "</li>" + "</br>");
document.write("</ol>");
document.write("</div>");

document.write(x += y - x++ * z);

document.write("</div>");

//Друге рівняння
x = 6;
y = 14;
z = 4;
document.write("<div class='second'>");

document.write("Другий приклад" + "</br>");
document.write("z = --x - y * 5" + "</br>");

document.write("<div class='explanation'>");
document.write("<ol>");
document.write("<li>" + "Перша дія --х, так як це префіксний декремент, то х відразу стає 6-1=5 " + "</li>" + "</br>");
document.write("<li>" + "Друга дія у*5, 14*5=70" + "</li>" + "</br>");
document.write("<li>" + "Третя дія від z=5-70=-65. Результат <span>-65</span>" + "</li>" + "</br>");
document.write("</ol>");
document.write("</div>");

document.write(z = --x - y * 5);
document.write("</div>");

//Третє рівняння
x = 6;
y = 14;
z = 4;
document.write("<div class='third'>");

document.write("Третій приклад" + "</br>");
document.write("y /= x + 5 % z" + "</br>");

document.write("<div class='explanation'>");
document.write("<ol>");
document.write("<li>" + "Перша дія 5%z, це буде 1 (залишок від ділення)" + "</li>" + "</br>");
document.write("<li>" + "Друга дія х+1=6+1=7" + "</li>" + "</br>");
document.write("<li>" + "/= це оператор присвоєння з діленням, тобто у=14/7=2. Результат <span>2</span>" + "</li>" + "</br>");
document.write("</ol>");
document.write("</div>");

document.write(y /= x + 5 % z);
document.write("</div>");

//Чесверте рівняння
x = 6;
y = 14;
z = 4;
document.write("<div class='fourth'>");

document.write("Четвертий приклад" + "</br>");
document.write("z - x++ + y * 5" + "</br>");

document.write("<div class='explanation'>");
document.write("<ol>");
document.write("<li>" + "Перша дія х++, але так як це постфіксний інкремент х залишаеться без зміни" + "</li>" + "</br>");
document.write("<li>" + "Друга дія y*5=14*5=70" + "</li>" + "</br>");
document.write("<li>" + "Третя дія 4-6=-2 (так як z=4 a x залишився без зміни, тобто х=6)" + "</li>" + "</br>");
document.write("<li>" + "до результату третьої дії додаємо результат 2 дії, -2+70=68 , результат <span>68</span>" + "</li>" + "</br>");
document.write("</ol>");
document.write("</div>");

document.write(z - x++ + y * 5);
document.write("</div>");

//П'яте рівняння
x = 6;
y = 14;
z = 4;
document.write("<div class='fifth'>");

document.write("П'ятий приклад" + "</br>");
document.write("x = y - x++ * z" + "</br>");

document.write("<div class='explanation'>");
document.write("<ol>");
document.write("<li>" + "Перша дія х++, але так як це постфіксний інкремент х залишаеться без зміни" + "</li>" + "</br>");
document.write("<li>" + "Друга дія x*z, так як х залишився незмінним, 6*4=24" + "</li>" + "</br>");
document.write("<li>" + "Третя дія х=у-результат другої дії, х=14-24=-10. Результат <span>-10</span>" + "</li>" + "</br>");
document.write("</ol>");
document.write("</div>");

document.write(x = y - x++ * z);
document.write("</div>");
